<?php
if (!empty($messages)) {
    print('<div id="messages">');
    // Выводим все сообщения.
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
<form action="" method="POST">
  Имя:<br>
  <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" /><br>
  E-mail:<br>
  <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" /><br>
  Год рождения:<select name="year">
  <?php for ($i = 1910; $i <2020; $i++) { ?>
  <option value="<?php print $i;?>" <?= $i == $values['year'] ? 'selected' : '' ?>><?= $i;?></option>
  <?php } ?>
  <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year']; ?>
  </select><br>
  Пол:<br>
   <input type="radio"  checked="checked" name="gender" value="муж" <?= "муж" == $values['gender'] ? 'checked="checked"' : '' ?>/>
   мужской
   <input type="radio" name="gender" value="жен" <?= "жен" == $values['gender'] ? 'checked="checked"' : '' ?>/>
    женский<br>
   Количество конечностей:<br>
	<input type="radio"  checked="checked" name="konech" value="4" <?= $values['konech'] == "4" ? 'checked="checked"' : '' ?>/>4
	<input type="radio" name="konech" value="5" <?= $values['konech'] == "5" ? 'checked="checked"' : '' ?>/>5
	<input type="radio" name="konech" value="6" <?= $values['konech'] == "6" ? 'checked="checked"' : '' ?>/>6<br>
	 Сверхспособности:<br>
	 <select name="abilities[]" multiple <?php if ($errors['abilities']) {print 'class="error"';} ?>>
        <?php  
        foreach ($abilities as $key => $value) {
            $sel = empty($values['abilities'][$key]) ? '' : ' selected="selected"';
            printf('<option value="%s"%s>%s</option>', $key, $sel, $value);
        }
        ?>
         </select><br>
     Биография:<br>
     <textarea name="bio" <?php if ($errors['bio']) {print 'class="error"';} ?> ><?php print $values['bio']; ?></textarea><br>
     С контрактом ознакомлен:<br>
	 <input type="checkbox" name="che" <?php if ($errors['che']) {print 'class="error"';} ?> <?= $values['che'] == "on" ? 'checked="checked"' : '' ?> /> С контрактом ознакомлен<br>
  <input type="submit" value="ok" />
</form>
