<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/
$user = 'u20347';
$passw = '7338582';
// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']))
{
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="My site"');
    print('<h1>401 Требуется авторизация</h1>');
    exit();
}
else {
    $dbh = new PDO('mysql:host=localhost;dbname=u20347', $user, $passw,
        array(PDO::ATTR_PERSISTENT => true));
    $sth = $dbh->prepare("SELECT pass FROM admins WHERE login = ?");
    $sth->execute(array($_SERVER['PHP_AUTH_USER']));
    $value = $sth->fetch(PDO::FETCH_COLUMN);
    if ($_SERVER['PHP_AUTH_USER'] != 'u20347' || md5($_SERVER['PHP_AUTH_PW']) != $value)
    {
        header('HTTP/1.1 401 Unanthorized');
        header('WWW-Authenticate: Basic realm="My site"');
        print('<h1>401 Требуется авторизация</h1>');
        exit();
    }
}
if (!empty($_GET['logout'])){
    header('Location: http://logout@u20347.kubsu-dev.ru/shest/admin.php/');
    exit();
}

if (!empty($_GET['delete'])){
    $stmt = $dbh->prepare("DELETE FROM application WHERE ID = ?");
    $stmt->execute(array($_GET['delete']));
    print("Успешно удалено!");
}


//print('Вы успешно авторизовались и видите защищенные паролем данные.');

// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********
$stmt = $dbh->query("SELECT * FROM application");
?>
<html>
	<head>
	 <style>
	 body{
   background-color:#66CDAA; 
 }
	 
 table {
	border-collapse: collapse; background-color:#FFB6C1;
}

table tr,th{
	border: 2px solid black;  text-align: center;
}

table td{
	border: 2px solid black;
}
 </style>
	
	</head>
	<body>
  <h1>Здравствуйте, администратор!</h1>
  <form method="GET">
  <input type="hidden" value="1" name="logout">
  <input type="submit" value="Выйти">
  </form>
    <table>
      <tr>
      	<td>id</td>
        <td>fio</td>
        <td>email</td>
        <td>year</td>
        <td>gender</td>
        <td>konech</td>
        <td>abilities</td>
        <td>bio</td>
        <td>che</td>
        <td>login</td>
        <td>pass</td>
      </tr>
      <tr>
      <?php 
        foreach($stmt as $row){
          ?><tr>
          		<td><?php echo $row['id'] ?></td>
                <td><?php echo $row['fio'] ?></td>
                <td><?php echo $row['email']?></td>
                <td><?php echo $row['year'] ?></td>
                <td><?php echo $row['gender'] ?></td>
                <td><?php echo $row['konech'] ?></td>
                <td><?php echo $row['abilities'] ?></td>
                <td><?php echo $row['bio'] ?></td>
                <td><?php echo $row['che'] ?></td>
                <td><?php echo $row['login'] ?></td>
                <td><?php echo $row['pass'] ?></td>
                
                <td><form method="GET"><input type="submit"  value="Удалить данные"></input> 

                <input type="hidden" value=<?php echo $row['id']?> name="delete"></form></td>
            </tr><?php
}
        ?>        
      </tr>
    </table>
  </body>
</html>
