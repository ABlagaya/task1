<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="style.css">
</head>

<body>
  <div class="container flexContainer">
    <h2>Форма</h2>
  </div>
</body>

</html>
<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
$abilities = ['bes' => 'Бессмертие','proh' => 'Проход сквозь стены','lev' => 'Левитация'];
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['pass'])) {
        $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
            strip_tags($_COOKIE['login']),
            strip_tags($_COOKIE['pass']));
    }
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['name'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);
  $errors['limbs'] = !empty($_COOKIE['konech_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);
  
  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['name']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('fio_error', '', 100000);
    // Выводим сообщение.
    if($_COOKIE['fio_error'] == '1') {
        $messages[] = '<div class="error">Заполните имя.</div>';
    }
    else {
        $messages[] = '<div class="error">Укажите корректное имя на русском языке.</div>';
    }
  }
  
  if ($errors['email']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('email_error', '', 100000);
      // Выводим сообщение.
      if($_COOKIE['email_error'] == '1') {
      $messages[] = '<div class="error">Заполните e-mail.</div>';
      }
      else {
          $messages[] = '<div class="error">Укажите корректный e-mail.</div>';
      }
  }
  if ($errors['year']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('year_error', '', 100000);
      // Выводим сообщение.
      if($_COOKIE['year_error'] == '1') {
          $messages[] = '<div class="error">Укажите год.</div>';
      }
      else {
          $messages[] = '<div class="error">Укажите корректный год.</div>';
      }
  }
  
  if ($errors['gender']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('gender_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Укажите пол.</div>';
  }
  
  if ($errors['limbs']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('konech_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Укажите количество конечностей.</div>';
  }
  
  if ($errors['abilities']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('$abilities_error', '', 100000);
      // Выводим сообщение.
      if($_COOKIE['abilities_error'] == '1') {
          $messages[] = '<div class="error">Выберите способность.</div>';
      }
      else {
          $messages[] = '<div class="error">Укажите корректную способность.</div>';
      }
  }
  
  if ($errors['biography']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('biography_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Заполните биографию.</div>';
  }
  if ($errors['checkbox']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('checkbox_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Согласитесь с контрактом.</div>';
  }
  
  
  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['name'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
  $values['year'] = empty($_COOKIE['year_value']) ? '' : strip_tags($_COOKIE['year_value']);
  $values['gender'] = empty($_COOKIE['gender_value']) ? '' : strip_tags($_COOKIE['gender_value']);
  $values['limbs'] = empty($_COOKIE['konech_value']) ? '' : strip_tags($_COOKIE['konech_value']);
  if (!empty($_COOKIE['abilities_value'])) {
      $abilities_value = json_decode(strip_tags($_COOKIE['abilities_value']));
  }
  $values['abilities'] = [];
  if(isset($abilities_value) && is_array($abilities_value)) {
      foreach ($abilities_value as $abil) {
          if(!empty($abilities[$abil])) {
          $values['abilities'][$abil] = $abil;
          }
      }
  }
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);
  $values['checkbox'] = empty($_COOKIE['checkbox_value']) ? '' : strip_tags($_COOKIE['checkbox_value']);
  // TODO: аналогично все поля.
  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
      $user = 'u20347';
      $passw = '7338582';
      $dbh = new PDO('mysql:host=localhost;dbname=u20347', $user, $passw,
          array(PDO::ATTR_PERSISTENT => true));
      $sth = $dbh->prepare("SELECT name FROM application WHERE login = ?");
      $sth->execute(array($_SESSION['login']));
      $value = $sth->fetch(PDO::FETCH_COLUMN);
      $sth = $dbh->prepare("SELECT email FROM application WHERE login = ?");
      $sth->execute(array($_SESSION['login']));
      $value1 = $sth->fetch(PDO::FETCH_COLUMN);
      $sth = $dbh->prepare("SELECT year FROM application WHERE login = ?");
      $sth->execute(array($_SESSION['login']));
      $value2 = $sth->fetch(PDO::FETCH_COLUMN);
      $sth = $dbh->prepare("SELECT gender FROM application WHERE login = ?");
      $sth->execute(array($_SESSION['login']));
      $value3 = $sth->fetch(PDO::FETCH_COLUMN);
      $sth = $dbh->prepare("SELECT limbs FROM application WHERE login = ?");
      $sth->execute(array($_SESSION['login']));
      $value4 = $sth->fetch(PDO::FETCH_COLUMN);
      $sth = $dbh->prepare("SELECT abilities FROM application WHERE login = ?");
      $sth->execute(array($_SESSION['login']));
      $value5 = $sth->fetch(PDO::FETCH_COLUMN);
      $sth = $dbh->prepare("SELECT biography FROM application WHERE login = ?");
      $sth->execute(array($_SESSION['login']));
      $value6 = $sth->fetch(PDO::FETCH_COLUMN);
      $sth = $dbh->prepare("SELECT checkbox FROM application WHERE login = ?");
      $sth->execute(array($_SESSION['login']));
      $value7 = $sth->fetch(PDO::FETCH_COLUMN);
      
      $values['name']=$value;
      $values['email']=$value1;
      $values['year']=$value2;
      $values['gender']=$value3;
      $values['limbs']=$value4;
      if (!empty($value5)) {
          $abilities_value = json_decode(strip_tags($value5));
      }
      $values['abilities'] = [];
      if(isset($abilities_value) && is_array($abilities_value)) {
          foreach ($abilities_value as $abil) {
              if(!empty($abilities[$abil])) {
                  $values['abilities'][$abil] = $abil;
              }
          }
      }
      $values['biography']=$value6;
      $values['checkbox']=$value7;
      // TODO: загрузить данные пользователя из БД
      // и заполнить переменную $values,
      // предварительно санитизовав.
      printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
  }
  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['name'])) {
    // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
      if (!preg_match('/^[а-яА-Я ]+$/u',$_POST['name'])) {
          setcookie('fio_error', '2', time() + 24 * 60 * 60);
          $errors = TRUE;
      }
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('fio_value', $_POST['name'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['email'])) {
      // Выдаем куку на день с флажком об ошибке в поле name.
      setcookie('email_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      if (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) === false) {
          setcookie('email_error', '2', time() + 24 * 60 * 60);
          $errors = TRUE;
      }
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['year'])) {
      // Выдаем куку на день с флажком об ошибке в поле name.
      setcookie('year_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      if (!(is_numeric($_POST['year']) && intval($_POST['year']) >= 1800 && intval($_POST['year']) <= 2020)) {
          setcookie('year_error', '2', time() + 24 * 60 * 60);
          $errors = TRUE;
      }
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('year_value', $_POST['year'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['gender'])) {
      // Выдаем куку на день с флажком об ошибке в поле name.
      setcookie('gender_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('gender_value', $_POST['gender'], time() + 12*30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['limbs'])) {
      // Выдаем куку на день с флажком об ошибке в поле name.
      setcookie('konech_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('konech_value', $_POST['limbs'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['abilities'])) {
      // Выдаем куку на день с флажком об ошибке в поле abilities.
      setcookie('abilities_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      $abilities_error = FALSE;
      foreach ($_POST['abilities'] as $a) {
          if (empty($abilities[$a])) {
              setcookie('abilities_error', '2', time() + 24 * 60 * 60);
              $errors = TRUE;
              $abilities_error = TRUE;
          }
      }
      // Сохраняем ранее введенное в форму значение на год.
      if (!$abilities_error) {
      setcookie('abilities_value', json_encode($_POST['abilities']), time() + 12 * 30 * 24 * 60 * 60);
      }
  }
  
  if (empty($_POST['biography'])) {
      // Выдаем куку на день с флажком об ошибке в поле name.
      setcookie('biography_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('biography_value', $_POST['biography'], time() + 12 * 30 * 24 * 60 * 60);
  }
  if (!isset($_POST['checkbox'])) {
      // Выдаем куку на день с флажком об ошибке в поле name.
      setcookie('checkbox_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('checkbox_value', $_POST['checkbox'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('year_error', '', 100000);
    setcookie('gender_error', '', 100000);
    setcookie('konech_error', '', 100000);
    setcookie('abilities_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('checkbox_error', '', 100000);
    
    // TODO: тут необходимо удалить остальные Cookies.
  }
  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
          // TODO: перезаписать данные в БД новыми данными,
          // кроме логина и пароля.
          // Сохранение в XML-документ.
             $user = 'u20347';
             $passw = '7338582';
             $db = new PDO('mysql:host=localhost;dbname=u20347', $user, $passw,
             array(PDO::ATTR_PERSISTENT => true));
  
  // Подготовленный запрос. Не именованные метки.
  try {
      $stmt = $db->prepare("UPDATE application SET name = ?, email = ?, year = ?, gender = ?, limbs = ?, abilities = ?, biography = ?, checkbox = ? WHERE login = ?");
      $stmt ->execute([$_POST['name'], $_POST['email'], $_POST['year'] , $_POST['gender'], $_POST['limbs'],  json_encode($_POST['abilities']), $_POST['biography'], $_POST['checkbox'], $_SESSION['login'] ] );
  }
  catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
      }
  }
  else {
      // Генерируем уникальный логин и пароль.
      // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
      $login = substr(md5(uniqid(rand(),1)), 0, 5);
      $pass = substr(md5(rand()), 0, 8);
      // Сохраняем в Cookies.
      setcookie('login', $login);
      setcookie('pass', $pass);
      
      // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
      // ...
      $user = 'u20347';
      $passw = '7338582';
      $db = new PDO('mysql:host=localhost;dbname=u20347', $user, $passw,
          array(PDO::ATTR_PERSISTENT => true));
      
      // Подготовленный запрос. Не именованные метки.
      try {
          $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, year = ?, gender = ?, limbs = ?, abilities = ?, biography = ?, checkbox = ?, login = ?, pass = ?");
          $stmt ->execute([$_POST['name'], $_POST['email'], $_POST['year'] , $_POST['gender'], $_POST['limbs'],  json_encode($_POST['abilities']), $_POST['biography'], $_POST['checkbox'], $login, md5($pass)  ] );
      }
      catch(PDOException $e){
          print('Error : ' . $e->getMessage());
          exit();
      }
      
  }

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: ./');
}
