<?php
if (!empty($messages)) {
    print('<div id="messages">');
    // Выводим все сообщения.
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
<form action="" method="POST">
  Имя:<br>
  <input name="name" <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>" /><br>
  E-mail:<br>
  <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" /><br>
  Год рождения:<select name="year">
  <?php for ($i = 1800; $i <2020; $i++) { ?>
  <option value="<?php print $i;?>" <?= $i == $values['year'] ? 'selected' : '' ?>><?= $i;?></option>
  <?php } ?>
  <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year']; ?>
  </select><br>
  Пол:<br>
   <input type="radio"  checked="checked" name="gender" value="мужской" <?= "мужской" == $values['gender'] ? 'checked="checked"' : '' ?>/>
   мужскойской
   <input type="radio" name="gender" value="женский" <?= "женский" == $values['gender'] ? 'checked="checked"' : '' ?>/>
    женскийский<br>
   Количество конечностей:<br>
	<input type="radio"  checked="checked" name="limbs" value="1" <?= $values['limbs'] == "1" ? 'checked="checked"' : '' ?>/>1
	<input type="radio" name="limbs" value="2" <?= $values['limbs'] == "2" ? 'checked="checked"' : '' ?>/>2
	<input type="radio" name="limbs" value="3" <?= $values['limbs'] == "3" ? 'checked="checked"' : '' ?>/>3
	<input type="radio" name="limbs" value="4" <?= $values['limbs'] == "4" ? 'checked="checked"' : '' ?>/>4
	<input type="radio" name="limbs" value="5" <?= $values['limbs'] == "5" ? 'checked="checked"' : '' ?>/>5
	<input type="radio" name="limbs" value="6" <?= $values['limbs'] == "6" ? 'checked="checked"' : '' ?>/>6<br>
	 Сверхспособности:<br>
	 <select name="abilities[]" multiple <?php if ($errors['abilities']) {print 'class="error"';} ?>>
        <?php  
        foreach ($abilities as $key => $value) {
            $sel = empty($values['abilities'][$key]) ? '' : ' selected="selected"';
            printf('<option value="%s"%s>%s</option>', $key, $sel, $value);
        }
        ?>
         </select><br>
     Биография:<br>
     <textarea name="biography" <?php if ($errors['biography']) {print 'class="error"';} ?> ><?php print $values['biography']; ?></textarea><br>
     С контрактом ознакомлен:<br>
	 <input type="checkbox" name="checkbox" <?php if ($errors['checkbox']) {print 'class="error"';} ?> <?= $values['checkbox'] == "on" ? 'checked="checked"' : '' ?> /> С контрактом ознакомлен<br>
  <input type="submit" value="Отправить" />
</form>
