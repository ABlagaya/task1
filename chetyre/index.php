<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="style.css">
</head>

<body>
  <div class="container flexContainer">
    <h2>Форма</h2>
  </div>
</body>

</html>
<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
$abilities = ['bes' => 'Бессмертие','proh' => 'Проходение сквозь стены','lev' => 'Левитация'];

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

	
  // В суперглобальном массиве $_COOKIE PHP хранит все параметры, переданные в текущем запросе через URL.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
	// Удаляем куку, указывая время устаревания в прошлом.
	setcookie('save', '', 100000);
	// Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }
  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);
  
  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['name']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('name_error', '', 100000);
    // Выводим сообщение.
    if($_COOKIE['name_error'] == '1') {
        $messages[] = '<div class="error">Заполните имя.</div>';
    }
    else {
        $messages[] = '<div class="error">Укажите корректное имя на русском языке.</div>';
    }
  }  
  if ($errors['email']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('email_error', '', 100000);
      // Выводим сообщение.
      if($_COOKIE['email_error'] == '1') {
      $messages[] = '<div class="error">Заполните e-mail.</div>';
      }
      else {
          $messages[] = '<div class="error">Укажите корректный e-mail.</div>';
      }
  }
  if ($errors['year']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('year_error', '', 100000);
      // Выводим сообщение.
      if($_COOKIE['year_error'] == '1') {
          $messages[] = '<div class="error">Укажите год.</div>';
      }
      else {
          $messages[] = '<div class="error">Укажите корректный год.</div>';
      }
  }  
  if ($errors['gender']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('gender_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Укажите пол.</div>';
  }  
  if ($errors['limbs']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('limbs_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Укажите количество конечностей.</div>';
  }
  if ($errors['abilities']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('$abilities_error', '', 100000);
      // Выводим сообщение.
      if($_COOKIE['abilities_error'] == '1') {
          $messages[] = '<div class="error">Выберите способность.</div>';
      }
      else {
          $messages[] = '<div class="error">Укажите корректную способность.</div>';
      }
  }  
  if ($errors['biography']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('biography_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Заполните биографию.</div>';
  }
  if ($errors['checkbox']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('checkbox_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Согласитесь с контрактом.</div>';
  }
  
  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
  $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
  if (!empty($_COOKIE['abilities_value'])) {
  $abilities_value = json_decode($_COOKIE['abilities_value']);
  }
  $values['abilities'] = [];
  if(isset($abilities_value) && is_array($abilities_value)) {
      foreach ($abilities_value as $abil) {
          if(!empty($abilities[$abil])) {
          $values['abilities'][$abil] = $abil;
          }
      }
  }
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
  $values['checkbox'] = empty($_COOKIE['checkbox_value']) ? '' : $_COOKIE['checkbox_value'];
  // TODO: аналогично все поля.  
  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
	// Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['name'])) {
    // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('name_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
      if (!preg_match('/^[а-яА-Я ]+$/u',$_POST['name'])) {
          setcookie('fio_error', '2', time() + 24 * 60 * 60);
          $errors = TRUE;
      }
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('name_value', $_POST['name'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['email'])) {
      // Выдаем куку на день с флажком об ошибке в поле email.
      setcookie('email_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      if (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) === false) {
          setcookie('email_error', '2', time() + 24 * 60 * 60);
          $errors = TRUE;
      }
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['year'])) {
      // Выдаем куку на день с флажком об ошибке в поле year.
      setcookie('year_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      if (!(is_numeric($_POST['year']) && intval($_POST['year']) >= 1800 && intval($_POST['year']) <= 2020)) {
          setcookie('year_error', '2', time() + 24 * 60 * 60);
          $errors = TRUE;
      }
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('year_value', $_POST['year'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['gender'])) {
      // Выдаем куку на день с флажком об ошибке в поле gender.
      setcookie('gender_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('gender_value', $_POST['gender'], time() + 12*30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['limbs'])) {
      // Выдаем куку на день с флажком об ошибке в поле limbs.
      setcookie('limbs_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('limbs_value', $_POST['limbs'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['abilities'])) {
      // Выдаем куку на день с флажком об ошибке в поле abilities.
      setcookie('abilities_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      $abilities_error = FALSE;
      foreach ($_POST['abilities'] as $a) {
          if (empty($abilities[$a])) {
              setcookie('abilities_error', '2', time() + 24 * 60 * 60);
              $errors = TRUE;
              $abilities_error = TRUE;
          }
      }
      // Сохраняем ранее введенное в форму значение на год.
      if (!$abilities_error) {
      setcookie('abilities_value', json_encode($_POST['abilities']), time() + 12 * 30 * 24 * 60 * 60);
      }
  }
  
  if (empty($_POST['biography'])) {
      // Выдаем куку на день с флажком об ошибке в поле biography.
      setcookie('biography_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('biography_value', $_POST['biography'], time() + 12 * 30 * 24 * 60 * 60);
  }
  if (!isset($_POST['checkbox'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('checkbox_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('checkbox_value', $_POST['checkbox'], time() + 12 * 30 * 24 * 60 * 60);
  }
// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************
  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('name_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('year_error', '', 100000);
    setcookie('gender_error', '', 100000);
    setcookie('limbs_error', '', 100000);
    setcookie('abilities_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('checkbox_error', '', 100000);  
    // TODO: тут необходимо удалить остальные Cookies.
  }
  // Сохранение в XML-документ.
  $user = 'u20347';
  $pass = '7338582';
  $db = new PDO('mysql:host=localhost;dbname=u20347', $user, $pass,
      array(PDO::ATTR_PERSISTENT => true));
  
  // Подготовленный запрос. Не именованные метки.
  try {
      $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, year = ?, gender = ?, limbs = ?, abilities = ?, biography = ?, checkbox = ?");
      $stmt ->execute([$_POST['name'], $_POST['email'], $_POST['year'] , $_POST['gender'], $_POST['limbs'],  json_encode($_POST['abilities']), $_POST['biography'], $_POST['checkbox']] );
  }
  catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
  }
  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');
  // Делаем перенаправление.
  header('Location: index.php');
}
