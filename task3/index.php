<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="style.css">
</head>

<body>
  <div class="container flexContainer">
    <h2>Форма</h2>
  </div>
</body>

</html>
<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;
if (empty($_POST['name'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}
else if(!preg_match('/^[а-яА-Я ]+$/u',$_POST['name'])){
    print('Недопустимые символы в имени.<br/>');
    $errors=TRUE;
}
if (empty($_POST['email'])) {
    print('Заполните e-mail.<br/>');
    $errors = TRUE;
}
else if(!preg_match('/^[a-zA-Z0-9@. ]+$/u',$_POST['email'])){
    print('Недопустимые символы в e-mail.<br/>');
    $errors=TRUE;
}
if (empty($_POST['year'])) {
    print('Заполните год.<br/>');
    $errors = TRUE;
}
else {
    $year=$_POST['year'];
    if(!(is_numeric($year)&& intval($year)>=1800 && intval($year)<=2020)){
    print('Укажите корректный год.<br/>');
    $errors=TRUE;
}
}
if (empty($_POST['gender'])) {
    print('Выберите пол.<br/>');
    $errors = TRUE;
}
if (!isset($_POST['limbs'])) {
    print('Выберите количество конечностей.<br/>');
    $errors = TRUE;
}
if (empty($_POST['biography'])) {
    print('Заполните биографию.<br/>');
    $errors = TRUE;
}
if (!isset($_POST['consent'])) {
    print('Поставьте согласие с условиями.<br/>');
    $errors = TRUE;
}
if (empty($_POST['abilities'])) {
    print('Выберите способность.<br/>');
    $errors = TRUE;
}
else{
    $abilities = $_POST['abilities'];
    foreach($abilities as $ability) {
        if (!in_array($ability, ['bes', 'proh', 'lev'])) {
            print('Плохая способность.<br/>');
            $errors = TRUE;
        }
    }
}
// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
// *************

if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

// Сохранение в базу данных.

$user = 'u20347';
$pass = '7338582';
$db = new PDO('mysql:host=localhost;dbname=u20347', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
try {
    $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, year = ?, gender = ?, limbs = ?, abilities = ?, biography = ?, consent = ?");
    $stmt ->execute([$_POST['name'], $_POST['email'], intval($year) , $_POST['gender'], $_POST['limbs'],  json_encode($abilities), $_POST['biography'], $_POST['consent']] );
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));
 
//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');
