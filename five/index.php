<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
$abils = ['god' => 'Бессмертие','noclip' => 'Проход сквозь стены','fly' => 'Левитация'];
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['mail'] = !empty($_COOKIE['mail_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['pol'] = !empty($_COOKIE['pol_error']);
  $errors['konec'] = !empty($_COOKIE['konec_error']);
  $errors['abils'] = !empty($_COOKIE['abils_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);
  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if (!empty($errors['fio'])) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('fio_error', '', 100000);
      // Выводим сообщение.
      if($_COOKIE['fio_error'] == '1') {
          $messages[] = '<div class="error">Заполните имя.</div>';
      }
      else {
          $messages[] = '<div class="error">Укажите корректное имя (Кириллицей).</div>';
      }
  }
  
  if (!empty($errors['mail'])) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('mail_error', '', 100000);
      // Выводим сообщение.
      if($_COOKIE['mail_error'] == '1') {
          $messages[] = '<div class="error">Заполните e-mail.</div>';
      }
      else {
          $messages[] = '<div class="error">Укажите корректный e-mail.</div>';
      }
  }
  if (!empty($errors['year'])) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('year_error', '', 100000);
      // Выводим сообщение.
      if($_COOKIE['year_error'] == '1') {
          $messages[] = '<div class="error">Укажите год.</div>';
      }
      else {
          $messages[] = '<div class="error">Укажите корректный год.</div>';
      }
  }
  
  if (!empty($errors['pol'])) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('pol_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Укажите пол.</div>';
  }
  
  if (!empty($errors['konec'])) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('konec_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Укажите количество конечностей.</div>';
  }
  
  if (!empty($errors['abils'])) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('$abils_error', '', 100000);
      // Выводим сообщение.
      if($_COOKIE['abils_error'] == '1') {
          $messages[] = '<div class="error">Выберите хотя бы 1 способность.</div>';
      }
      else {
          $messages[] = '<div class="error">Укажите корректную способность.</div>';
      }
  }
  
  if (!empty($errors['bio'])) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('bio_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Заполните биографию.</div>';
  }
  if (!empty($errors['check'])) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('check_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Согласитесь с контрактом.</div>';
  }
  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  // При этом санитизуем все данные для безопасного отображения в браузере.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  $values['mail'] = empty($_COOKIE['mail_value']) ? '' : strip_tags($_COOKIE['mail_value']);
  $values['year'] = empty($_COOKIE['year_value']) ? '' : strip_tags($_COOKIE['year_value']);
  $values['pol'] = empty($_COOKIE['pol_value']) ? '' : strip_tags($_COOKIE['pol_value']);
  $values['konec'] = empty($_COOKIE['konec_value']) ? '' : strip_tags($_COOKIE['konec_value']);
  if (!empty($_COOKIE['abils_value'])) {
      $abils_value = json_decode(strip_tags($_COOKIE['abils_value']));
  }
  $values['abils'] = [];
  if(isset($abils_value) && is_array($abils_value)) {
      foreach ($abils_value as $abil) {
          if(!empty($abils[$abil])) {
              $values['abils'][$abil] = $abil;
          }
      }
  }
  $values['bio'] = empty($_COOKIE['bio_value']) ? '' : strip_tags($_COOKIE['bio_value']);
  $values['check'] = empty($_COOKIE['check_value']) ? '' : strip_tags($_COOKIE['check_value']);
  // TODO: аналогично все поля.
  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])){
          $user = 'u20336';
          $passw = '2383966';
          $dbh = new PDO('mysql:host=localhost;dbname=u20336', $user, $passw,
              array(PDO::ATTR_PERSISTENT => true));
          $sth = $dbh->prepare("SELECT name FROM application WHERE login = ?");
          $sth->execute(array($_SESSION['login']));
          $value = $sth->fetch(PDO::FETCH_COLUMN);
          $sth = $dbh->prepare("SELECT email FROM application WHERE login = ?");
          $sth->execute(array($_SESSION['login']));
          $value1 = $sth->fetch(PDO::FETCH_COLUMN);
          $sth = $dbh->prepare("SELECT year FROM application WHERE login = ?");
          $sth->execute(array($_SESSION['login']));
          $value2 = $sth->fetch(PDO::FETCH_COLUMN);
          $sth = $dbh->prepare("SELECT pol FROM application WHERE login = ?");
          $sth->execute(array($_SESSION['login']));
          $value3 = $sth->fetch(PDO::FETCH_COLUMN);
          $sth = $dbh->prepare("SELECT konec FROM application WHERE login = ?");
          $sth->execute(array($_SESSION['login']));
          $value4 = $sth->fetch(PDO::FETCH_COLUMN);
          $sth = $dbh->prepare("SELECT abilities FROM application WHERE login = ?");
          $sth->execute(array($_SESSION['login']));
          $value5 = $sth->fetch(PDO::FETCH_COLUMN);
          $sth = $dbh->prepare("SELECT biography FROM application WHERE login = ?");
          $sth->execute(array($_SESSION['login']));
          $value6 = $sth->fetch(PDO::FETCH_COLUMN);
          $sth = $dbh->prepare("SELECT checkbox FROM application WHERE login = ?");
          $sth->execute(array($_SESSION['login']));
          $value7 = $sth->fetch(PDO::FETCH_COLUMN);
          
          $values['fio']=$value;
          $values['mail']=$value1;
          $values['year']=$value2;
          $values['pol']=$value3;
          $values['konec']=$value4;
          if (!empty($value5)) {
              $abils_value = json_decode(strip_tags($value5));
          }
          $values['abils'] = [];
          if(isset($abils_value) && is_array($abils_value)) {
              foreach ($abils_value as $abil) {
                  if(!empty($abils[$abil])) {
                      $values['abils'][$abil] = $abil;
                  }
              }
          }
          $values['bio']=$value6;
          $values['check']=$value7;
          // TODO: загрузить данные пользователя из БД
          // и заполнить переменную $values,
          // предварительно санитизовав.
          printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
      }
  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['fio'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('fio_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      if (!preg_match('/^[а-яА-Я ]+$/u',$_POST['fio'])) {
          setcookie('fio_error', '2', time() + 24 * 60 * 60);
          $errors = TRUE;
      }
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('fio_value', $_POST['fio'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['mail'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('mail_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      if (filter_var($_POST["mail"], FILTER_VALIDATE_EMAIL) === false) {
          setcookie('mail_error', '2', time() + 24 * 60 * 60);
          $errors = TRUE;
      }
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('mail_value', $_POST['mail'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['year'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('year_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      if (!(is_numeric($_POST['year']) && intval($_POST['year']) >= 1900 && intval($_POST['year']) <= 2020)) {
          setcookie('year_error', '2', time() + 24 * 60 * 60);
          $errors = TRUE;
      }
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('year_value', $_POST['year'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['pol'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('pol_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('pol_value', $_POST['pol'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['konec'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('konec_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('konec_value', $_POST['konec'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['abils'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('abils_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      $abils_error = FALSE;
      foreach ($_POST['abils'] as $a) {
          if (empty($abils[$a])) {
              setcookie('abils_error', '2', time() + 24 * 60 * 60);
              $errors = TRUE;
              $abils_error = TRUE;
          }
      }
      // Сохраняем ранее введенное в форму значение на год.
      if (!$abils_error) {
          setcookie('abils_value', json_encode($_POST['abils']), time() + 12 * 30 * 24 * 60 * 60);
      }
  }
  
  if (empty($_POST['bio'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('bio_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('bio_value', $_POST['bio'], time() + 12 * 30 * 24 * 60 * 60);
  }
  if (!isset($_POST['check'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('check_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('check_value', $_POST['check'], time() + 12 * 30 * 24 * 60 * 60);
  }

// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('mail_error', '', 100000);
    setcookie('year_error', '', 100000);
    setcookie('pol_error', '', 100000);
    setcookie('konec_error', '', 100000);
    setcookie('abils_error', '', 100000);
    setcookie('bio_error', '', 100000);
    setcookie('check_error', '', 100000);
    
    // TODO: тут необходимо удалить остальные Cookies.
  }

  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: перезаписать данные в БД новыми данными,
    // кроме логина и пароля.
          $user = 'u20336';
          $passw = '2383966';
          $db = new PDO('mysql:host=localhost;dbname=u20336', $user, $passw,
              array(PDO::ATTR_PERSISTENT => true));
          
          // Подготовленный запрос. Не именованные метки.
          try {
              $stmt = $db->prepare("UPDATE application SET name = ?, email = ?, year = ?, pol = ?, konec = ?, abilities = ?, biography = ?, checkbox = ? WHERE login = ?");
              $stmt ->execute([$_POST['fio'], $_POST['mail'], $_POST['year'] , $_POST['pol'], $_POST['konec'],  json_encode($_POST['abils']), $_POST['bio'], $_POST['check'], $_SESSION['login']  ] );
          }
          catch(PDOException $e){
              print('Error : ' . $e->getMessage());
              exit();
          }
  }
  else {
    // Генерируем уникальный логин и пароль.
    // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
    $login = substr(md5(uniqid(rand(),1)), 0, 5);
    $pass = substr(md5(rand()), 0, 8);
    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('pass', $pass);

    // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
    // ...
    $user = 'u20336';
    $passw = '2383966';
    $db = new PDO('mysql:host=localhost;dbname=u20336', $user, $passw,
        array(PDO::ATTR_PERSISTENT => true));
    
    // Подготовленный запрос. Не именованные метки.
    try {
        $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, year = ?, pol = ?, konec = ?, abilities = ?, biography = ?, checkbox = ?, login = ?, pass = ?");
        $stmt ->execute([$_POST['fio'], $_POST['mail'], $_POST['year'] , $_POST['pol'], $_POST['konec'],  json_encode($_POST['abils']), $_POST['bio'], $_POST['check'], $login, md5($pass)  ] );
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
  }

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: ./');
}
